@extends('layout.master')

@section('judul')
    Halaman Tambah Cast
@endsection

@section('isi')

<div>
    <form action="/genre" method="POST">
            @csrf
            <div class="form-group">
                <label for="title">Nama Cast</label>
                <input type="text" class="form-control" name="nama" placeholder="Masukkan Nama Cast"><br>
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
                <label for="title">Umur</label>
                <input type="text" class="form-control" name="umur" placeholder="Masukkan Umur Anda"><br>
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
                <label for="title">Bio</label><br>
                <textarea type="text" name="bio" cols="30" rows="10"></textarea>
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
    </form>
</div>

@endsection