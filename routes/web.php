<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/test/{angka}', function($angka) {
//     return view('test', ["angka" => $angka]);
// });

// Route::get('/halo/{nama}', function($nama) {
//     return "Halo $nama";
// });

// Route::get('/form', "RegisterController@form");

// Route::get('/sapa', "RegisterController@sapa");

// Route::post('/sapa', "RegisterController@sapa_post");

// Route::get('/', 'HomeController@home');
// Route::get('/register', 'AuthController@register');
// Route::post('/welcome', 'AuthController@welcome');

// Route::get('/master', function() {
//     return view('/layout.master');
// });

// Route::get('/home2', 'HomeController@home2');
// Route::get('/form', 'FormController@form');
// Route::post('post', 'FormController@kirim');

// Route::get('/datatable', function() {
//     return view('table.datatable');
// });

// Route::get('/table', function() {
//     return view('table.table');
// });
// Route::get('/genre', 'genreController@index');
// Route::get('/genre/create', 'genreController@create');
// Route::post('/genre', 'genreController@store');

Route::get('/cast', 'CastController@index');
Route::get('/cast/create', 'CastController@create');
Route::post('cast', 'CastController@store');
Route::get('cast/{cast_id}', 'CastController@show');
Route::get('cast/{cast_id}/edit', 'CastController@edit');
Route::put('cast/{cast_id}', 'CastController@update');
Route::delete('cast/{cast_id}', 'CastController@destroy');


