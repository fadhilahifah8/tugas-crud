<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class genreController extends Controller
{
    public function index() {
        //return view('genre.create');
    }

    public function create() {
        return view('genre.create');
    }

    public function store(Request $request) {
        $request->validate([
            'nama' => 'required|unique:genre'
        ]);
        $query = DB::table('genre')->insert([
            "nama" => $request["nama"]
        ]);
        return redirect('/genre');
    }
}
